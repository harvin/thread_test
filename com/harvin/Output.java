package com.harvin;

public class Output {
	private boolean flag = false;
	private static int i = 0;
	
	public synchronized void output_odd(){
		for (; i < 100; i++) {			
			try {
				if (flag){				
					System.out.println("odd :"+i);
					flag = false;
					notifyAll();
				}
				wait();
			} catch (Exception e) {
				
			}
		}
	}
	
	public synchronized void output_even(){
		for (; i < 100; i++) {
			try {
				if (!flag) {
					System.out.println("even :"+i);
					flag = true;
					notifyAll();
				}
				wait();
			} catch (Exception e) {
				
			}
		}
	}
}


class Odd extends Thread{
	private Output o;
	public Odd(Output o){
		this.o = o;
	}
	public void run(){
		o.output_odd();
	}
}
class Even extends Thread{
	private Output o;
	public Even(Output o){
		this.o = o;
	}
	public void run(){
		o.output_even();
	}
}